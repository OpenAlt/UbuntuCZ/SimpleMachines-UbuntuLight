<?php
echo '
    <div id="header">
        <div class="logo"><a href="/" title=""> <img src="', $settings['theme_url'], '/images/logo.svg" alt="Ubuntu" /></a></div>

        <ul class="menu">
            <li><a href="/" title="Fórum">Domů</a></li>
            <li><a href="https://www.ubuntu.cz/" title="Web Ubuntu CZ/SK">Ubuntu CZ/SK</a></li>
            <li><a href="https://wiki.ubuntu.cz/" title="Návody na wiki Ubuntu CZ/SK">Wiki</a></li>
            <li><a href="https://www.ubuntu.cz/komunita/" title="Informace o komunitě Ubuntu CZ/SK">Komunita</a></li>
        </ul>

        <div class="search">
            <form id="search_form" action="', $scripturl, '?action=search2" method="post" accept-charset="', $context['character_set'], '">
                <input type="text" name="search" value="" class="input_text" />
                <input type="submit" name="submit" value="', $txt['search'], '" class="button_submit" />
                <input type="hidden" name="advanced" value="0" />';
if (!empty($context['current_topic'])) { // Search within current topic?
    echo '
                <input type="hidden" name="topic" value="', $context['current_topic'], '" />';
} elseif (!empty($context['current_board'])) { // If we're on a certain board, limit it to this board ;).
    echo '
                <input type="hidden" name="brd[', $context['current_board'], ']" value="', $context['current_board'], '" />';
}
echo '
            </form>
        </div>
    </div>';

echo '
    <div id="menu-header">
        <div id="menu">', template_menu(), '</div>
    </div>';
